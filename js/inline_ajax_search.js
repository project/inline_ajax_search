var resultsShown = false; var countCharsShown = false; var IAS_el; var IAS_el_res;
Drupal.behaviors.inline_ajax_search = function() {
	IAS_el = $('#inline_ajax_search_container #inline_ajax_search');
  IAS_el_res = $('#inline_ajax_search_container #inline_ajax_search_results');
  IAS_el.val(IAS_el.attr('title'));
  IAS_el_res.hide();
  IAS_el.keyup(function() {
    if($(this).val().length >= IAS_settings.word_size) {
			countCharsShown = false;
      var path = Drupal.settings.basePath + 'admin/settings/search/inline/get/';
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: path + $(this).val(),
        success: inline_ajax_search_succes
      });
      return false;
		} else if($(this).val().length > 0) {
			if(!countCharsShown) {
				resultsShown = true;
				countCharsShown = true;
	      var result = '<div class="searchresult lowcharcount">' + Drupal.t('You have to search with more than ') + IAS_settings.word_size + Drupal.t(' characters.') + '</div>';
				IAS_el_res.empty();
	      IAS_el_res.append(result);
				IAS_el_res.fadeIn('fast');
        IAS_el_res.addClass('shown');
			}
    } else {
      resultsShown = false;
			countCharsShown = false;
      IAS_el_res.fadeOut('fast', function() {
        IAS_el_res.removeClass('shown');
        IAS_el_res.empty();
      });
    }
  });
  
  IAS_el.focus(function() {
    if($(this).val() == $(this).attr('title')) {
      $(this).val('');
    }
  });
  
  IAS_el.blur(function(e) {
    if(!resultsShown) {
      $(this).val($(this).attr('title'));
    }
  });
  
  $('body').click(function(e) {
    if(resultsShown) {
      var left = IAS_el_res.offset().left;
      var top = IAS_el_res.offset().top;
      var bottom = top + IAS_el_res.height();
      var right = left + IAS_el_res.width();
      if(e.pageX > left && e.pageX < right && e.pageY > top && e.pageY < bottom) {
        // do something inside the results
      } else {
        resultsShown = false;
        IAS_el_res.fadeOut('fast', function() {
          IAS_el_res.empty();
          IAS_el_res.removeClass('shown');
          IAS_el.val(IAS_el.attr('title'));
        });
      }
    }
  });
}

function inline_ajax_search_succes(data) {
  if(data.answer != null) {
    resultsShown = true;
    IAS_el_res.html(data.answer);
    IAS_el_res.fadeIn('fast');
    IAS_el_res.addClass('shown');
  }
}
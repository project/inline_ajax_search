<?php
/**
 * @file
 * The file that holds the function that does the search
 */
function _inline_ajax_search($str) {
  #$types = variable_get('inline_ajax_search_types', array());
  $types = array();
  $nr = variable_get('inline_ajax_search_count', 10);
  if (count($types) > 0) {
    $str .= ' type:';
    foreach ($types as $type) {
      if (!empty($type)) {
        $str .= $type . ',';
      }
    }
  }
  $results = luceneapi_node_search('search', $str . '*');
  $count = count($results);
  if ($count > 0) {
    $n = 0;
    foreach ($results as $result) {
      if ($n < $nr) {
        $ret .= '<div class="searchresult"><h3>' . l($result['title'], $result['link']) . '</h3>';
        if(variable_get('inline_ajax_search_snippet', 0) == 1) {
          $ret .= '<span>' . $result['snippet'] . '</span>';
        }
        $ret .= '</div>';
        $n++;
      }
    }
    if ($count > $nr) {
      $ret .= '<div class="moreresults">' . l(t('Found') . ' ' . $count . ' ' . t('results. ') . t('Show more >>'), 'search/luceneapi_node/' . $str . '*') . '</div>';
    }
  } else {
  	$ret = t('<div class="searchresult noresults">No results were found. Try with a different term.</div>');
  }
  return drupal_json(array('answer' => $ret));
}
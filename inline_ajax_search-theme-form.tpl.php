<?php

/**
 * 
 * @file inline_ajax_search-theme-form.tpl.php
 * @see template_preprocess_inline_ajax_search_theme_form()
 */
?>
<div id="inline_ajax_search_container">
  <input type="text" name="inline_ajax_search" id="inline_ajax_search" title="<?php print t('type a keyword'); ?>">
  <div id="inline_ajax_search_results"></div>
</div>
